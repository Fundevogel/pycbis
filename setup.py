# ~*~ coding=utf-8 ~*~

import io
from os.path import abspath, dirname, join
from setuptools import find_packages, setup


VERSION = '0.1'


def long_description():
    readme_file = join(dirname(abspath(__file__)), 'README.md')

    with io.open(readme_file, encoding='utf8') as file:
        return file.read()


setup(
    name='pycbis',
    description='Python CLI utility and library for connecting to pcbis.de API',
    long_description=long_description(),
    long_description_content_type='text/markdown',
    version=VERSION,
    license='MIT',
    author='Martin Folkers',
    author_email='hello@twobrain.io',
    maintainer='Fundevogel',
    maintainer_email='maschinenraum@fundevogel.de',
    url='https://github.com/Fundevogel/pycbis',
    project_urls={
        "Source code": "https://github.com/Fundevogel/pycbis",
        "Issues": "https://github.com/Fundevogel/pycbis/issues",
    },
    packages=find_packages(),
    install_requires=[
        'click',
        'isbnlib',
        'xdg',
        'zeep',
    ],
    entry_points='''
        [console_scripts]
        pycbis=pycbis.cli:cli
    ''',
    python_requires='>=3.0',
)
