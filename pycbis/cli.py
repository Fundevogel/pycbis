from getpass import getpass
from hashlib import md5
from os import getcwd
from os.path import join

import click

from .config import Config
from .api.exceptions import InvalidLoginException
from .api.webservice import Webservice
from .utils import load_json, dump_json

clickpath = click.Path(exists=True)
pass_config = click.make_pass_decorator(Config, ensure=True)


def ask_credentials():
    return {
        'VKN': getpass('VKN: '),
        'Benutzer': getpass('Benutzer: '),
        'Passwort': getpass('Passwort: '),
    }


@click.group()
@pass_config
@click.option('-v', '--verbose', is_flag=True, default=None, help='Activate verbose mode.')
@click.option('--cache-dir', type=clickpath, help='Custom cache directory.')
def cli(config, verbose,  cache_dir):
    """CLI utility for connecting to pcbis.de API"""

    # Apply CLI options
    if verbose is not None:
        config.verbose = verbose

    if cache_dir is not None:
        config. cache_dir =  cache_dir


# GENERAL tasks

@cli.command()
@pass_config
def version(config):
    """Check current API version"""

    # Initialize webservice
    ws = Webservice()

    try:
        click.echo('Current API version: ' + ws.version())

    except Exception as error:
        click.echo('Error: ' + str(error))


@cli.command()
@pass_config
@click.argument('isbn')
@click.option('-o', '--output-file', type=click.Path(), help='Path to output JSON file.')
@click.option('-c', '--cache-only', is_flag=True, help='Only return cached database records.')
@click.option('-f', '--force-refresh', is_flag=True, help='Force database record being updated.')
@click.option('--credentials', type=clickpath, help='Path to JSON file containing credentials.')
def lookup(config, isbn, output_file, cache_only, force_refresh, credentials):
    """Lookup information about ISBN"""

    if cache_only is False:
        if credentials is not None:
            credentials = load_json(credentials)

        else:
            click.echo('Please enter your account information first:')
            credentials = ask_credentials()

    click.echo('Loading data ..', nl=False)

    data = {}

    try:
        # Initialize webservice
        ws = Webservice(credentials, config.cache_dir)

    except InvalidLoginException as error:
        click.echo(' failed!')

        click.echo('Authentication error: ' + str(error))
        click.Context.exit(1)

    # Retrieve data (either from cache or via API call)
    data = ws.fetch(isbn, force_refresh)

    click.echo(' done!')

    if config.verbose:
        click.echo(data)

    else:
        if 'AutorSachtitel' in data:
            click.echo('Match: ' + data['AutorSachtitel'])

    if output_file:
        dump_json(data, output_file)
        click.echo('Data saved: ' + output_file)


@cli.command()
@pass_config
@click.argument('isbn')
@click.option('-q', '--quantity', default=1, help='Number of items to be checked.')
@click.option('--credentials', type=clickpath, help='Path to JSON file containing credentials.')
def ola(config, isbn, quantity, credentials):
    if credentials is not None:
        credentials = load_json(credentials)

    else:
        click.echo('Please enter your account information first:')
        credentials = ask_credentials()

    click.echo('Calling OLA ..', nl=False)

    try:
        # Initialize webservice
        ws = Webservice(credentials, config.cache_dir)

    except InvalidLoginException as error:
        click.echo(' failed!')

        click.echo('Authentication error: ' + str(error))
        click.Context.exit(1)

    # Retrieve data (either from cache or via API call)
    ola = ws.ola(isbn, int(quantity))

    click.echo(' done!')

    if config.verbose:
        click.echo(ola.data)

    else:
        click.echo(str(ola))
