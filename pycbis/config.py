from configparser import SafeConfigParser
from os.path import isfile, join

from xdg import xdg_config_home, xdg_data_home


class Config(object):
    def __init__(self):
        # Provide sensible defaults
        config = SafeConfigParser()
        config['DEFAULT'] = {
            'verbose': 'off',
        }

        config['directories'] = {
            'cache_dir': join(xdg_data_home(), 'pycbis'),
        }

        # Load config provided by user
        config_file = join(xdg_config_home(), 'pycbis', 'config')

        if isfile(config_file):
            config.read(config_file)

        # Apply resulting config
        self.config = config

        for section in config.sections():
            for option in config[section]:
                setattr(self, option, self.get(section, option))


    def get(self, section: str, option: str):
        booleans = ['verbose']

        if self.config.has_option(section, option):
            if option in booleans:
                return self.config.getboolean(section, option)

            return self.config.get(section, option)

        if option in self.config['DEFAULT']:
            return self.config.get('DEFAULT', option)

        raise Exception
