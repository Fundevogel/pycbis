import json
from os import makedirs
from os.path import dirname, exists


# JSON tasks

def load_json(json_file) -> dict:
    data = {}

    try:
        with open(json_file, 'r') as file:
            data = json.load(file)

    except json.decoder.JSONDecodeError:
        raise Exception

    except FileNotFoundError:
        pass

    return data


def dump_json(data, json_file) -> None:
    create_path(json_file)

    with open(json_file, 'w') as file:
        json.dump(data, file, ensure_ascii=False, indent=4)


# HELPER functions

def create_path(file_path) -> None:
    if not exists(dirname(file_path)):
        try:
            makedirs(dirname(file_path))

        # Guard against race condition
        except OSError:
            pass
